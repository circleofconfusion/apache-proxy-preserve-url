package com.knudsen.proxyexperiment;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;
import javax.servlet.http.HttpServletRequest; 

@RestController
public class InfoController {

    @RequestMapping("/server-name")
    public String getServerName(HttpServletRequest request) {
        return request.getServerName();
    }
}
